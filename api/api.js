import request from '@/utils/request.js'
import upload from '@/utils/upload.js'

export function openChat(data) {
    return request({
        url: '/utils/openChat',
        method: 'post',
        data: data
    })
}