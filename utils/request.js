import {
	tansParams
} from '@/utils/ruoyi'
import {
	baseApi
} from '@/utils/common'

let baseURL = baseApi;
const timeout = 50000;

function request(config) {
	// uni.showLoading({
	// 	title: '获取中'
	// });
	const access_token = uni.getStorageSync('token')
	if (access_token) {
		config.header = {};
		config.header['Authorization'] = 'Bearer ' + access_token;
		if (config.contentType) {
			config.header['Content-Type'] = config.contentType
		}else{
			config.header['Content-Type'] = "application/json"
		}
	}
	if (config.params) {
		let url = config.url + '?' + tansParams(config.params)
		url = url.slice(0, -1)
		config.params = {}
		config.url = url
	}
	if (config.url.substr(0, 3) == "htt") {
		baseURL = ''
	}

	return new Promise((resolve, reject) => {
		uni.request(Object.assign(config, {
			url: baseURL + config.url,
			method: !config.method ? 'GET' : config.method.toLocaleUpperCase(),
			timeout: timeout, //超时时间
			success: res => {
				uni.hideLoading();
				if (res.data.code == 200) {
					resolve(res.data);
				} else {
					// uni.showToast({
					// 	title: res.data.msg,
					// 	icon: 'none'
					// });
					reject(res.data);
				}
			},
			fail: (error) => {
				uni.hideLoading();
				uni.showToast({
					title: '网络错误',
					icon: 'error'
				});
				reject(error);
			}
		}));
	})
}
export default request
