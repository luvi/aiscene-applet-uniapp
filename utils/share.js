import {
	baseApi
} from '@/utils/common'
export default {
	data() {
		return {

		}
	},
	onLoad: function() {
		wx.showShareMenu({
			withShareTicket: true,
			menus: ["shareAppMessage", "shareTimeline"]
		})
	},
	onShareAppMessage(res) {
		let that = this;
		let imageUrl = that.shareUrl || '';
		// let path = `/` + that.$scope.route + `?options=` + that.$scope.options;
		return {
			title: 'AI Scene',
			path: '/pages/index/index',
			imageUrl: '../../static/images/share.jpg'
		};
	},
	// 分享到朋友圈
	onShareTimeline() {
		return {
			title: 'AI Scene',
			path: '/pages/index/index',
			imageUrl: '../../static/images/b965cee4-bb5f-4c63-a8e4-299bc2c4252b.png'
		};
	},
	methods: {

	}
}
