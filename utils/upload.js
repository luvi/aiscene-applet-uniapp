import {
	tansParams
} from '@/utils/ruoyi'
import {
	baseApi
} from '@/utils/common'

let baseURL = baseApi;
const timeout = 5000;

function upload(config) {
	const access_token = uni.getStorageSync('token')
	return new Promise((resolve, reject) => {
		uni.uploadFile(Object.assign(config, {
			url: baseURL + config.url,
			method: !config.method ? 'GET' : config.method.toLocaleUpperCase(),
			header: {
				"Authorization": 'Bearer ' + access_token
			},
			success: res => {
				if (JSON.parse(res.data).code == 200) {
					resolve(JSON.parse(res.data));
				} else {
					uni.showToast({
						title: JSON.parse(res.data).msg,
						icon: 'none'
					});
					reject(JSON.parse(res.data));
				}
			},
			fail: (error) => {
				uni.showToast({
					title: '网络错误，请重试',
					icon: 'none'
				});
				reject(error);
			}
		}));
	})
}
export default upload
